<?php
header("Access-Control-Allow-Origin: *"); 
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS'); 
header('Access-Control-Allow-Headers: Authorization, X-Access-Token, X-Requested-With, accept, Content-Type, Content-Range, Content-Disposition, Content-Description'); 
header("Access-Control-Allow-Credentials: true"); 
header('Content-Type: application/json');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1.0/', 'namespace' => 'Api'], function () {
    Route::post('user/login', 'UsersController@login');
    Route::post('user/forgotpassword', 'UsersController@forgotPassword');
});


Route::group(['prefix' => 'v1.0/', 'middleware' => ['api'], 'namespace' => 'Api'], function () {
    Route::get('user/logout', 'UsersController@logout');
    Route::get('getclient', 'AllController@getCleint');
    Route::get('getclient-type', 'AllController@getCleintType');
    Route::get('get-recouncilation', 'AllController@getReCouncilationData');
    Route::post('user/profile', 'UsersController@profile');
    Route::post('get-files', 'GettignFileController@getFileUploadedBySftp');
    Route::post('uploadfiles','AllController@upload');
    Route::get('get-dataimport', 'GettignFileController@getDataImport');
});