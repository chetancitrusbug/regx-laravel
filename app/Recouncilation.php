<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Recouncilation extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'MifidIITxnRec.Reconciliation';
	
	protected $primaryKey = 'ReconciliationID';
    // const CREATED_AT = 'CreateDateTime';
	// const UPDATED_AT = 'ModifiedDateTime';
	// const api_token = 'API_TOKEN';
	// protected $fillable = [
    //     'name', 'email', 'password', 'API_TOKEN'
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];
}
