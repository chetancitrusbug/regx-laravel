<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
class UsersController extends Controller
{
    public function register(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = 'Registration Successful, Please Check your email for activate your account.';
        $status = 'true';
        $error = '';
    }

    public function login(Request $request)
    {
        
        $data = array();
        $code = 200;
        $messages = 'Login Successfull';
        $status = 'true';
        $error = '';
        
        $email = $request->email;
        $password = $request->password;
        $rules = array(
            'email' => 'required',
            'password' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        
        //dd($validator->passes(), $validator->messages()->toArray());
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        
        } else {
         
            $user = User::where('UserName', $email)->leftJoin('Main.Client', 'Client.ClientID', 'User.ClientID')->where('UserPassword',$password)->first();
            if ($user) {
             
                //if (Hash::check($password, $user->UserPassword)) {
					$user->API_Token = md5(uniqid());
                    $user->update();
                    $data = $user;
                    $messages = 'Login Success';
                //} else {
                //    $messages = 'Password is wrong';
                //    $code = 400;
                //    $status = 'false';
                //}
             
            } else {
                $messages = 'User Name not exits, Please Contact Admin';
                $code = 400;
                $status = 'false';
            }
         
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function logout(Request $request)
    {
        $data = [];
        $message = "";

        $code = 200;
        $status = true;
        if ($request->has('api_token')) {
            $api_token = $request->get('API_Token');
            User::where("API_Token", $api_token)->update([ "API_Token" => ""]);
        }
        $message = "Logout successfully";
        return response()->json(['result' => $data, 'status' => $status, 'message' => $message, 'code' => $code]);
    }

	 public function profile(Request $request)
    {	
		$api_token = $request->api_token;
        $UserID = $request->UserID;
        $rules = array(
            'api_token' => 'required',
            'UserID' => 'required',
        );
        $data = [];
        $message = "";

        $code = 200;
        $status = true;
		$validator = \Validator::make($request->all(), $rules, []);
        
        //dd($validator->passes(), $validator->messages()->toArray());
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        
        } else {
			$user = User::where('UserID', $UserID)->where('API_TOKEN',$api_token)->first();
			$data = $user;
			$messages = "User Detail Successfull";
		}
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
	
    public function forgotPassword(Request $request)
    {
        $data = array();
        $code = 200;
        $messages = '';
        $status = 'true';
        $error = '';
        $email = $request->email;
        $user = User::where('UserName', $email)->first();

        if ($user) {
            $response = Password::sendResetLink($request->only('UserName'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            if ($response == "passwords.sent") {
                $messages = 'Password reset link has been sent to email';
            } else if ($response == "passwords.user") {
                $status = false;
                $messages = 'User not found';
                $code = 400;
            } else {
                $status = false;
                $messages = 'Something went wrong ! Please try again later';
                $code = 400;
            }
        } else {
            $messages = 'UserName not found';
            $code = 400;
            $status = 'false';
        }


        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function changePassword(Request $request)
    {
        $data = [];
        $messages = "";
        $status = true;

        $code = 200;
        $id = $request->user_id;
        $rules = array(
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|same:password',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::where("id", $id)->first();
            //dd($user);
            if ($user == null) {
                $messages = 'User not Found';
                $status = false;
                $code = 400;
            } else {
                if (Hash::check($request->input('password'), $user->UserPassword)) {
                    $messages = 'Current password and new password are same';
                    $status = false;
                    $code = 400;
                } else {
                    $user->UserPassword = Hash::make($request->input('password'));
                    $user->save();
                    $messages = 'Password changed successfully.';
                }
            }
        }

        return response()->json(['status' => $status, 'result' => $data, 'message' => $messages, 'code' => $code]);
        exit;
    }
}
?>