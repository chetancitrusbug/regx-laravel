<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Recouncilation;
use DB;
use Schema;

class GettignFileController extends Controller
{

    public function getFileUploadedBySftp(Request $request){

    }

    public function getDataImport(Request $request){
        $data = [];
        $message = "Data get Successfully";
        $code = 200;
        $status = true;
        $page = 10;
        if ($request->has('api_token')) {
            // $columns = Schema::getColumnListing('Reconciliation');
            // $data["columns"] = $columns;

            //$recouncilation = Recouncilation::where('IsActive', 1)->get();
            //$data["recouncilation"] = $recouncilation;

            $from_date = strtolower(date('d-M-y', strtotime($request->from_date)));
            $to_date = strtolower(date('d-M-y', strtotime($request->to_date)));
            $Client = strtolower($request->clientId);
            $db = DB::connection()->getPdo();
            $stmt = $db->prepare("EXEC Main.DisplayDataImportStatus ?,?,?");
            $stmt->bindParam(1, $Client);
            $stmt->bindParam(2, $from_date);
            $stmt->bindParam(3, $to_date);
			if ($stmt->execute()) {
                $recouncilation = $stmt->fetchAll(\PDO::FETCH_CLASS, 'stdClass');
                $data = $recouncilation;
            } else {

                $status = false;
                $code = 400;
                $message = 'Something Went Wrong';
            }
        } else {
            $message = 'Please Try again.';
            $code = 400;
            $status = false;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $message, 'status' => $status]);
        exit;
    }
}
