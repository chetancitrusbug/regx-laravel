<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Recouncilation;
use DB;
use Schema;
class AllController extends Controller
{
    public function upload(Request $request){
        $data = [];
        $message = "File Upload Successfully";

        $code = 200;
        $status = true;
        $api_token = $request->api_token;
        
        //$recouncilationType = $request->recouncilationType;
        $rules = array(
            'api_token' => 'required',
            //'recouncilationType' => 'required',
        );
        
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];

        } else {
            
            $fileType = "Flat";
            $user = User::where('API_TOKEN', $api_token)->first();
            if($user){
                if (!is_dir(public_path().'/upload/' . $user->UserName)) {
                    mkdir(public_path() . '/upload/' . $user->UserName, 0777, true);
                }
                $imagename = $_FILES["file"]['name'];
                $explodeName = explode('_', $_FILES["file"]['name']);
                $fromclient = $explodeName[0];
                $toclient = $explodeName[1];
                $imagename = $fromclient.'_'.$toclient.'_'.time().'_'. $explodeName[2];
                $filetype = $_FILES["file"]["type"];

                $userName = $user->UserName;
                $date = date('Y-m-d h:m:i');
                if($this->getCleint($toclient) && $this->getCleint($fromclient)){
                    $imagePath = public_path() . '/upload/' . $user->UserName . '/' . $imagename;
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $imagePath)) {
                        $activeUser = 1;
                        $db = DB::connection()->getPdo();
                        $stmt = $db->prepare("EXEC Main.InsertUpdateIntoFileTable ?,?,?,?,?,?,?,?");
                        //$stmt->bindParam(1, $recouncilationType);
                        $stmt->bindParam(1, $fileType);
                        $stmt->bindParam(2, $fromclient);
                        $stmt->bindParam(3, $toclient);
                        $stmt->bindParam(4, $userName);
                        $stmt->bindParam(5, $imagename);
                        $stmt->bindParam(6, $imagePath);
                        $stmt->bindParam(7, $date);
                        if(!$stmt->execute()){
                            $status = false;
                            $code = 400;
                            $message = 'Something Went Wrong';
                        }
                    }
                }else{
                    $status = false;
                    $code = 400;
                    $message = 'Client Not Found, Please Try again';
                }

            }
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $message, 'status' => $status]);
        exit;
    }

    public function getReCouncilationData(Request $request){
        
        $data = [];
        $message = "Data get Successfully";
        $code = 200;
        $status = true;
        $page = 10;
        if ($request->has('api_token')) {
			/* @FileFromDate			DATE,
			@FileToDate			DATE,
			@Client				NVARCHAR(100),	--Client name
			@ReconciliationType NVARCHAR(100),	--firm-arm 
			@RecAttribute		NVARCHAR(100)	--RecordMatching */
            $FileFromDate = date('d-M-y', strtotime($request->FileFromDate));
            $FileToDate = date('d-M-y', strtotime($request->FileToDate));
			//dd($FileFromDate,$FileToDate);
			$Client = $request->clientName;
            $ReconciliationType = $request->ReconciliationType;
            $RecAttribute = $request->RecAttribute;
            $db = DB::connection()->getPdo();
            $stmt = $db->prepare("EXEC Main.TwoWayRecon ?,?,?,?,?");
            $stmt->bindParam(1, $FileFromDate);
            $stmt->bindParam(2, $FileToDate);
            $stmt->bindParam(3, $Client);
            $stmt->bindParam(4, $ReconciliationType);
            $stmt->bindParam(5, $RecAttribute);
           
            if($stmt->execute()){
                $recouncilation = $stmt->fetchAll(\PDO::FETCH_CLASS, 'stdClass');
                $data["recouncilation"] = $recouncilation;
                $recouncilationColumns = $recouncilation[0];
                foreach($recouncilationColumns as $key => $columns){
                    $data["columns"][] =  $key;
                }
            }else{
                
                $status = false;
                $code = 400;
                $message = 'Something Went Wrong';
            }
        }else{
            $message = 'Please Try again.';
            $code = 400;
            $status = false;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $message, 'status' => $status]);
        exit;
    }

    
    public function getCleint($client){
       $client = Client::where('IsActive', 1)->where('ClientName','=', $client)->get();
       if(count($client) > 0){
           return true;
       }
        return false;
    }

    public function getCleintType(Request $request){
        $data = [];
        $message = "Client Type Get Successfully";

        $code = 200;
        $status = true;
        if($request->has('api_token')){
            // SELECT
            //     ClientTypeID,
            //     ClientTypeName,
            //     CreateDateTime,
            //     CreateUser,
            //     ModifiedDateTime,
            //     ModifiedUser,
            //     IsActive
            //     FROM
            //     Main . ClientType
            $clientType = DB::select(DB::raw("
                     SELECT
                ClientTypeID,
                ClientTypeName,
                IsActive
                FROM
                Main . ClientType Where IsActive=1"));
            $data = $clientType;
        }else{
            $message = "Try After Some time!";
            $code = 400;
            $status = false;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $message, 'status' => $status]);
        exit;
    }
}
